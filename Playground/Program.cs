using System.Runtime.InteropServices.ComTypes;
using Playground;
using RabbitMQ.Client;
using RabbitMQ.Client.Extensions;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Json;
using Serilog.Sinks.SystemConsole.Themes;


Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
    .Enrich.FromLogContext()
    .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {SourceContext} {Message:lj}{NewLine}{Exception}" ,theme: AnsiConsoleTheme.Literate)
    .CreateLogger();

IHost host = Host.CreateDefaultBuilder(args)
    // .ConfigureLogging(bld => bld.AddSimpleConsole(cfg =>
    // {
    //     cfg.IncludeScopes = true;
    //     cfg.SingleLine = true;
    // }))
    .ConfigureServices(services =>
    {
        //services.AddHostedService<Worker>();
    })
    .UseSerilog()
    .Build();

await host.StartAsync();
var factory = new ConnectionFactory
{
    Uri = new Uri("amqp://localhost"),
    AutomaticRecoveryEnabled = true,
    TopologyRecoveryEnabled = false
};

var logger = host.Services.GetRequiredService<ILogger<Program>>();  

using (var connection = (IAutorecoveringConnection) factory.CreateConnection())
{
    logger.LogInformation("Connected success");
    connection.AddLogging(logger);
    
    
    connection.CallbackException += (_, e) => logger.LogError(e.Exception, nameof(connection.CallbackException));

    await using (var model = connection.CreateAutoRecoveringModel(logger,
                     initModel: mdl =>
                     {
                         mdl.ExchangeDeclare("test12", "direct");
                         mdl.ExchangeDeclare("test12", "fanout");
                     }))
    {
        await Task.Factory.StartNew(_ => Console.ReadKey(), null, TaskCreationOptions.LongRunning);
        await Task.Factory.StartNew(_ => Console.ReadKey(), null, TaskCreationOptions.LongRunning);
    }

    Console.WriteLine(2);
    Console.ReadKey();
    
}
await host.WaitForShutdownAsync();