﻿using System.Reactive.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Client.Extensions;

public static class ConnectionExtensions
{
    
    public static string ConnectionName(this IConnection connection)
    {
        connection.ClientProperties.TryGetValue("connection_name", out var name);
        return name?.ToString() ??  connection.Endpoint.ToString();
    }

    public static T AddLogging<T>(this T connection, ILogger logger)
        where T : IConnection
    {
        connection.ConnectionShutdown += (o, e) =>
        {
            switch (e.Initiator)
            {
                case ShutdownInitiator.Application:
                case ShutdownInitiator.Library:
                    logger.LogInformation("connection shutdown, initiator: {Initiator}", e.Initiator);
                    break;
                case ShutdownInitiator.Peer:
                    logger.LogError("connection shutdown, initiator: {Initiator}", e.Initiator);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(e.Initiator), e.Initiator, "unknown case");
            }
        };

        if (connection is not IAutorecoveringConnection autoRecoveringConnection)
            return connection;
        autoRecoveringConnection.ConnectionRecoveryError += (_, e)
            => logger.LogDebug(e.Exception, "reconnect error: {Message}", e.Exception.Message);

        autoRecoveringConnection.RecoverySucceeded += (_, e) => logger.LogInformation("connection recovered.");

        return connection;
    }



    

    public static RecoveringModel CreateAutoRecoveringModel(this IAutorecoveringConnection connection,
        ILogger? logger = null,
        Action<IModel>? initModel = null,
        Action<IModel>? disposeModel = null,
        TimeSpan recreateInterval = default)
    {
        initModel ??= _ => { };
        disposeModel ??=  _ => { };
        recreateInterval = recreateInterval <= TimeSpan.Zero ? TimeSpan.FromSeconds(3) : recreateInterval;
        logger ??= new NullLoggerFactory().CreateLogger("");
        return new RecoveringModel(connection, logger, initModel, disposeModel, recreateInterval);
    }
    
}








