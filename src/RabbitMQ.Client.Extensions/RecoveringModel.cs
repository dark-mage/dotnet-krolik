﻿using System.Threading.Channels;
using Microsoft.Extensions.Logging;

namespace RabbitMQ.Client.Extensions;

internal class RecoveringModel : IAsyncDisposable, IDisposable
{
    private readonly ILogger _logger;
    private readonly Action<IModel> _initModel;
    private readonly Action<IModel> _disposeModel;
    private readonly Channel<Msg> _mailBox = Channel.CreateUnbounded<Msg>();
    private readonly Task _loop;

    public RecoveringModel(IAutorecoveringConnection connection,
        ILogger logger,
        Action<IModel> initModel,
        Action<IModel> disposeModel,
        TimeSpan recreateInterval)
    {
        _logger = logger;
        _initModel = initModel;
        _disposeModel = disposeModel;
        
        _mailBox.Writer.TryWrite(new Msg.Create());
        _loop = Loop(connection, recreateInterval);
    }


    public async Task<T> ExecuteAsync<T>(Func<IModel, T> todo)
    {
        var source = new TaskCompletionSource<object?>();
        await _mailBox.Writer.WriteAsync(new Msg.Execute(mdl =>
            todo(mdl), source));
        return (T) (await source.Task)!;
    }

    public async Task ExecuteAsync(Action<IModel> todo)
    {
        var source = new TaskCompletionSource<object?>();
        await _mailBox.Writer.WriteAsync(new Msg.Execute(mdl =>
        {
            todo(mdl);
            return new object();

        }, source));
        await source.Task;
    }

    private async Task Loop(IAutorecoveringConnection connection, TimeSpan recreateInterval)
    {
        IModel? model = null;
        
        void OnConnectionRecovered(object? sender, EventArgs e)
            => _mailBox.Writer.TryWrite(new Msg.Create());

        void RecreateAfterInterval()
            => _ = Task.Run(async () =>
            {
                await Task.Delay(recreateInterval);
                await _mailBox.Writer.WriteAsync(new Msg.Create());
            });

        void CleanupModel()
        {
            model?.Dispose();
            if(model != null) _disposeModel(model);
            model = null;
        }
        
        connection.RecoverySucceeded += OnConnectionRecovered;
        try
        {
            await foreach (var msg in _mailBox.Reader.ReadAllAsync())
            {
                switch (msg)
                {
                    case Msg.Create _:
                    {
                        if (model?.IsOpen == true || !connection.IsOpen)
                            continue;
                        CleanupModel();
                        try
                        {
                            model = connection.CreateModel();
                            model.ModelShutdown += (_, e) =>
                            {
                                if (e.Initiator != ShutdownInitiator.Peer)
                                {
                                    _logger.LogInformation("model shutdown by {Initiator}", e.Initiator);
                                    return;
                                }

                                _logger.LogError("model shutdown by {Initiator}", e.Initiator);
                                RecreateAfterInterval();
                            };
                            _logger.LogInformation("model created");
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "error on create model");
                            RecreateAfterInterval();
                            continue;
                        }

                        try
                        {
                            _initModel(model);
                            _logger.LogInformation("model initialized");
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "error on init model");
                        }

                        break;
                    }
                    case Msg.Execute (var todo, var source):
                    {
                        if (model == null)
                        {
                            source.TrySetException(new ObjectDisposedException("model"));
                            continue;
                        }

                        try
                        {
                            source.TrySetResult(todo(model));
                        }
                        catch (Exception ex)
                        {
                            //model.Dispose();
                            //_disposeModel(model);
                            // _ = Task.Run(async () =>
                            // {
                            //     await Task.Delay(_recreateInterval);
                            //     await _mailBox.Writer.WriteAsync(new Msg.Create());
                            // });
                            source.TrySetException(ex);
                        }
                        break;
                    }
                }
            }
            CleanupModel();
        }
        finally
        {
            connection.RecoverySucceeded -= OnConnectionRecovered;
        }
    }

    private abstract record Msg
    {
        private Msg() {}
        public sealed record Create : Msg;

        public sealed record Execute(Func<IModel, object?> Todo, TaskCompletionSource<object?> Source) : Msg;
    }

    public async ValueTask DisposeAsync()
    {
        _mailBox.Writer.TryComplete();
        await _loop;
        _logger.LogDebug("disposed");
        
    }
    
    public void Dispose()
    {
        _mailBox.Writer.TryComplete();
        _loop.GetAwaiter().GetResult();
        _logger.LogDebug("disposed");
    }
}